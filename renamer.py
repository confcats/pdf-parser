import os
import csv
import re

# Change these to match your file paths
pdf_dir = "papers"
output_dir = "renamed_papers"
csv_file = "rename.csv"

print(f"\n=== PDF Renamer Starting ===")
print(f"Looking for PDFs in: {pdf_dir}")
print(f"Output directory: {output_dir}")
print(f"Using CSV file: {csv_file}\n")

# Create output directory if it doesn't exist
if not os.path.exists(output_dir):
    print(f"Creating output directory: {output_dir}")
    os.makedirs(output_dir)

# Open the CSV file and create a dictionary mapping old paths to new names
name_map = {}
try:
    with open(csv_file, "r") as csvfile:
        print("Reading CSV file...")
        reader = csv.DictReader(csvfile)  # Use DictReader since we have headers
        mapping_count = 0
        for row in reader:
            file_path = os.path.join(pdf_dir, row['file'])  # Get complete path using the 'file' column
            new_name = row['new_name']  # Get new name from 'new_name' column
            name_map[file_path] = new_name
            mapping_count += 1
        print(f"Found {mapping_count} name mappings in CSV\n")

except FileNotFoundError:
    print(f"Error: Could not find CSV file: {csv_file}")
    exit(1)

# Loop through the mappings from the CSV
print("Starting renaming process...")

files_processed = 0
files_renamed = 0
files_not_found = 0

for old_path, new_name in name_map.items():
    if os.path.exists(old_path):
        files_processed += 1
        new_path = os.path.join(output_dir, new_name)
        try:
            os.rename(old_path, new_path)
            print(f"✓ Renamed: {old_path} -> {new_path}")
            files_renamed += 1
        except OSError as e:
            print(f"✗ Error renaming {old_path}: {str(e)}")
    else:
        print(f"! File not found: {old_path}")
        files_not_found += 1

print(f"\n=== Renaming Complete ===")
print(f"Files processed: {files_processed}")
print(f"Files renamed: {files_renamed}")
print(f"Files not found: {files_not_found}\n")