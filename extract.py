import zipfile
import os

# Path to your zip file
zip_path = 'submissions.zip'

# Output directory where extracted files will be saved
output_directory = 'Submissions'
os.makedirs(output_directory, exist_ok=True)

# Open the zip file
with zipfile.ZipFile(zip_path, 'r') as zip_ref:
    # Get a list of all files in the zip archive
    all_files = zip_ref.namelist()
    
    # Extract the first 10 files
    first_10_files = all_files[:10]
    
    for file in first_10_files:
        # Extract each file
        zip_ref.extract(file, path=output_directory)
        print(f'Extracted: {file}')