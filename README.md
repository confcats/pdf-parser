# How to use
1. Clone repo
2. Download submissions into ./Submissions directory

# Install
- Install Poppler from pdf2image in Linux
```bash
sudo apt update
sudo apt install poppler-utils
```