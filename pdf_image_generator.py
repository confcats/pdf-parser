import os
import re
from PyPDF2 import PdfReader
from pdf2image import convert_from_path

# Ensure the 'images' directory exists
os.makedirs('images', exist_ok=True)

def sanitize_filename(filename):
    """Sanitize the filename to remove problematic characters."""
    # Replace backslashes and other non-alphanumeric characters with underscores
    return re.sub(r'[\\/:*?"<>|]', '_', filename)

curdir = os.getcwd()
report_data = []
header = ['File Path', 'Pass/Fail', 'Reason', 'Number of Pages', 'First Image', 'Last Image']
report_data.append(header)

for path, subdirs, files in os.walk(curdir):
    for file in files:
        validFile = True
        split_tup = os.path.splitext(file)
        if split_tup[1].lower() == '.pdf':
            filepath = os.path.join(path, file)

            # Normalize and sanitize the file path and name
            rel_path = os.path.relpath(filepath, curdir)
            path_parts = rel_path.split(os.sep)

            # Extract the ID from the directory structure and sanitize it
            try:
                submissions_index = path_parts.index('submissions')
                id = path_parts[submissions_index + 1]  # Get the [id] part
            except ValueError:
                print(f"Submissions directory not found in path: {rel_path}")
                continue
            except IndexError:
                print(f"Could not extract ID from path: {rel_path}")
                continue

            sanitized_id = sanitize_filename(id)
            sanitized_file_name = sanitize_filename(file)

            print(f"Processing file with ID: {sanitized_id}")

            try:
                reader = PdfReader(filepath)
                length = len(reader.pages)
            except Exception as exc:
                print(f"Could not open file {filepath}: {exc}")
                validFile = False
                length = 0

            reduced_path = filepath.replace(curdir, '')

            if validFile:
                try:
                    images_first = convert_from_path(
                        filepath, dpi=72, first_page=1, last_page=1, size=(1200, None)
                    )
                    images_last = convert_from_path(
                        filepath, dpi=72, first_page=length, last_page=length, size=(1200, None)
                    )

                    # Sanitize file names before saving
                    first_name = os.path.join('images', f'{sanitized_id}_first.jpg')
                    image_first = f'{sanitized_id}_first.jpg'
                    last_name = os.path.join('images', f'{sanitized_id}_last.jpg')
                    image_last = f'{sanitized_id}_last.jpg'

                    images_first[0].save(first_name, 'JPEG')
                    images_last[0].save(last_name, 'JPEG')
                except Exception as exc:
                    print(f"Error converting pages to images for file {filepath}: {exc}")
                    validFile = False
                    result = 'fail'
                    reason = 'error extracting images'
                    image_first = ''
                    image_last = ''
                else:
                    if length > 5:
                        result = 'fail'
                        reason = 'invalid pdf length'
                    else:
                        result = 'pass'
                        reason = ''
            else:
                result = 'fail'
                reason = 'could not open file'
                image_first = ''
                image_last = ''

            row = [reduced_path, result, reason, length, image_first, image_last]
            report_data.append(row)

with open('report.csv', 'w', encoding='UTF8', newline='') as f:
    writer = csv.writer(f)
    writer.writerows(report_data)
