import os
import csv
import fitz  # PyMuPDF

def get_pdf_info(pdf_path):
    doc = fitz.open(pdf_path)
    page = doc[0]  # Analyze first page for margins
    
    # Get PDF metadata
    metadata = doc.metadata
    print("Metadata:", metadata)  # This will help us see what's available
    
    # Try to get format from metadata
    pdf_format = metadata.get('format', 'Unknown')
    print("Format:", pdf_format)

    # Get page dimensions
    mediabox = page.mediabox
    page_width = round(mediabox.width, 2) # in pt
    page_height = round(mediabox.height, 2) # in pt
    width_mm = round(page_width * 0.352778, 1) # in mm
    height_mm = round(page_height * 0.352778, 1) # in mm
    dimensions = f"{width_mm}x{height_mm}mm"
    
    # Get content bounds
    content_bounds = page.bound()
    
    # Calculate margins
    left_margin = content_bounds.x0
    right_margin = page_width - content_bounds.x1
    top_margin = content_bounds.y0
    bottom_margin = page_height - content_bounds.y1
    
    avg_margin = round((left_margin + right_margin + top_margin + bottom_margin) / 4, 2)
    
    # Extract first and second text blocks
    first_text = ""
    second_text = ""
    blocks = page.get_text("blocks")

    # Truncated version of text blocks
    # if len(blocks) >= 1:
    #     first_text = blocks[0][4].strip()
    #     if len(first_text) > 100:
    #         first_text = first_text[:97] + "..."
            
    # if len(blocks) >= 2:
    #     second_text = blocks[1][4].strip()
    #     if len(second_text) > 100:
    #         second_text = second_text[:97] + "..."

    # Untruncated version of text blocks
    if len(blocks) >= 1:
        first_text = blocks[0][4].strip()
            
    if len(blocks) >= 2:
        second_text = blocks[1][4].strip()
    
    # Get text from last page
    last_page = doc[-1]
    last_page_blocks = last_page.get_text("blocks")
    last_page_text = ""
    
    if len(last_page_blocks) >= 1:
        last_page_text = last_page_blocks[0][4].strip()[:200]
    
    # Get number of pages
    num_pages = len(doc)
    
    doc.close()
    return num_pages, f"{avg_margin}pt", dimensions, first_text, second_text, last_page_text, pdf_format

def analyze_pdfs():
    papers_dir = "renamed_papers"
    output_file = "pdf_analysis.csv"
    
    # Ensure Submissions directory exists
    if not os.path.exists(papers_dir):
        print(f"Creating {papers_dir} directory...")
        os.makedirs(papers_dir)
    
    # Create/open CSV file
    with open(output_file, 'w', newline='', encoding='utf-8') as csvfile:
        csvwriter = csv.writer(csvfile)
        csvwriter.writerow(['File Name', 'Pages', 'Size', 'Margins', 'Dimensions', 
                          'First Text Block', 'Second Text Block', 'Last Page First Block',
                          'PDF Format'])
        
        # Process each PDF in the Submissions directory
        pdf_files = [f for f in os.listdir(papers_dir) if f.lower().endswith('.pdf')]
        
        if not pdf_files:
            print(f"No PDF files found in {papers_dir} directory!")
            return
            
        for filename in pdf_files:
            file_path = os.path.join(papers_dir, filename)
            
            try:
                # Get file size
                size_bytes = os.path.getsize(file_path)
                size_mb = round(size_bytes / (1024 * 1024), 2)
                
                # Get all information
                num_pages, margins, dimensions, first_text, second_text, last_page_text, pdf_format = get_pdf_info(file_path)
                
                # Write to CSV
                csvwriter.writerow([
                    filename,
                    num_pages,
                    f"{size_mb}MB",
                    margins,
                    dimensions,
                    first_text,
                    second_text,
                    last_page_text,
                    pdf_format
                ])
                
                print(f"Processed: {filename}")
                print(f"First text block: {first_text[:50]}...")
                print(f"Second text block: {second_text[:50]}...")
                print(f"Last page first block: {last_page_text[:50]}...")
                print("-" * 50)
                
            except Exception as e:
                print(f"Error processing {filename}: {str(e)}")

if __name__ == "__main__":
    print("PDF Analysis Script")
    print("-----------------")
    analyze_pdfs()
    print("-----------------")
    print("Analysis complete. Check pdf_analysis.csv for results.")